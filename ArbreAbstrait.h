#ifndef ARBREABSTRAIT_H
#define ARBREABSTRAIT_H

// Contient toutes les déclarations de classes nécessaires
//  pour représenter l'arbre abstrait

#include <vector>
#include <iostream>
#include <iomanip>
#include <map>
#include <boost/variant.hpp>

using namespace std;

#include "Symbole.h"
#include "Exceptions.h"
class Interpreteur;


////////////////////////////////////////////////////////////////////////////////

class Noeud {
    // Classe abstraite dont dériveront toutes les classes servant à représenter l'arbre abstrait
    // Remarque : la classe ne contient aucun constructeur
public:
    virtual boost::variant<int, std::string> executer() = 0; // Méthode pure (non implémentée) qui rend la classe abstraite

    virtual void ajoute(Noeud* instruction) {
        throw OperationInterditeException();
    }

    virtual ~Noeud() {
    } // Présence d'un destructeur virtuel conseillée dans les classes abstraites
    virtual void traduitEnCPP(ostream & out, unsigned int indentation)const = 0;
};

////////////////////////////////////////////////////////////////////////////////

class NoeudSeqInst : public Noeud {
    // Classe pour représenter un noeud "sequence d'instruction"
    //  qui a autant de fils que d'instructions dans la séquence
public:
    NoeudSeqInst(); // Construit une séquence d'instruction vide

    ~NoeudSeqInst() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute chaque instruction de la séquence
    void ajoute(Noeud* instruction); // Ajoute une instruction à la séquence
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    vector<Noeud *> m_instructions; // pour stocker les instructions de la séquence
};

////////////////////////////////////////////////////////////////////////////////

class NoeudAffectation : public Noeud {
    // Classe pour représenter un noeud "affectation"
    //  composé de 2 fils : la variable et l'expression qu'on lui affecte
public:
    NoeudAffectation(Noeud* variable, Noeud* expression); // construit une affectation

    ~NoeudAffectation() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute (évalue) l'expression et affecte sa valeur à la variable
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    Noeud* m_variable;
    Noeud* m_expression;
};

////////////////////////////////////////////////////////////////////////////////

class NoeudOperateurBinaire : public Noeud {
    // Classe pour représenter un noeud "opération binaire" composé d'un opérateur
    //  et de 2 fils : l'opérande gauche et l'opérande droit
public:
    NoeudOperateurBinaire(Symbole operateur, Noeud* operandeGauche, Noeud* operandeDroit);
    // Construit une opération binaire : operandeGauche operateur OperandeDroit

    ~NoeudOperateurBinaire() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute (évalue) l'opération binaire)
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    Symbole m_operateur;
    Noeud* m_operandeGauche;
    Noeud* m_operandeDroit;
};

////////////////////////////////////////////////////////////////////////////////

class NoeudInstSi : public Noeud {
    // Classe pour représenter un noeud "instruction si"
    //  et ses 4 fils : la condition du si,la séquence d'instruction associée, les éventuelles conditions et expression du SinonSi, et la sequence du sinon
public:
    NoeudInstSi(Noeud* condition, Noeud* sequence, vector<Noeud*> noeudsSinonsi, Noeud* sequenceSinon);
    // Construit une "instruction si" avec sa condition et sa séquence d'instruction

    ~NoeudInstSi() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction si : si condition vraie on exécute la séquence
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    Noeud* m_conditionSi;
    Noeud* m_sequenceSi;
    vector<Noeud*> m_v_sinonsi;
    Noeud* m_sequenceSinon;
};
////////////////////////////////////////////////////////////////////////////////

class NoeudInstTantQue : public Noeud {
public:
    NoeudInstTantQue(Noeud* condition, Noeud* sequence);

    ~NoeudInstTantQue() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction si : si condition vraie on exécute la séquence
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    Noeud* m_condition;
    Noeud* m_sequence;

};
////////////////////////////////////////////////////////////////////////////////

class NoeudInstRepeter : public Noeud {
public:
    NoeudInstRepeter(Noeud* condition, Noeud* sequence);

    ~NoeudInstRepeter() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction si : si condition vraie on exécute la séquence
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    Noeud* m_condition;
    Noeud* m_sequence;

};
////////////////////////////////////////////////////////////////////////////////

class NoeudInstPour : public Noeud {
public:
    NoeudInstPour(Noeud* affectation, Noeud* condition, Noeud* affectation2, Noeud* sequence);

    ~NoeudInstPour() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction si : si condition vraie on exécute la séquence
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    Noeud* m_condition;
    Noeud* m_affectation;
    Noeud* m_affectation2;
    Noeud* m_sequence;

};
////////////////////////////////////////////////////////////////////////////////

class NoeudInstEcrire : public Noeud {
public:
    NoeudInstEcrire();

    ~NoeudInstEcrire() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction ecrire
    void ajoute(Noeud* instruction);
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    vector<Noeud*> m_v_instructions;
};
////////////////////////////////////////////////////////////////////////////////

class NoeudInstLire : public Noeud {
public:
    NoeudInstLire();

    ~NoeudInstLire() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction ecrire
    void ajoute(Noeud* variable);
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    vector<Noeud*> m_v_variables;
};
////////////////////////////////////////////////////////////////////////////////

class NoeudInstSelon : public Noeud {
public:
    NoeudInstSelon(Noeud* variable, Noeud* instructionsDefaut, vector<Noeud*> & instructionsCas);

    ~NoeudInstSelon() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction ecrire
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
private:
    Noeud* m_variable;
    vector<Noeud*> m_v_instructionsCas;
    Noeud* m_instructionsDefaut;
};
////////////////////////////////////////////////////////////////////////////////

class NoeudInstProcedure : public Noeud {
public:
    NoeudInstProcedure();

    ~NoeudInstProcedure() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction ecrire
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
    void ajoute(Noeud* instruction);

    inline static const map<string, NoeudInstProcedure*> & getProcs() {
        return NoeudInstProcedure::m_procedures;
    }
    static void ajouter(string signature, NoeudInstProcedure* procedure);

    inline void setSignature(string& si) {
        this->m_signature = si;
    }

    inline void setNbArgs(const int i) {
        this->m_nbArgs = i;
    }

    inline const int getNbArgs() {
        return this->m_nbArgs;
    }
private:
    static map<string, NoeudInstProcedure*> m_procedures; // Conteneur associatif de toutes les procedures déclarées avec leurs signatures.
    vector<Noeud*> m_v_instructions;
    string m_signature;
    int m_nbArgs = 0;
};
////////////////////////////////////////////////////////////////////////////////

class NoeudInstAppel : public Noeud {
public:
    NoeudInstAppel();

    ~NoeudInstAppel() {
    } // A cause du destructeur virtuel de la classe Noeud
    boost::variant<int, std::string> executer(); // Exécute l'instruction ecrire
    void traduitEnCPP(ostream & out, unsigned int indentation)const;
    void cherche(string& modele); // chercher fonction correspondant à la signature
    void ajouter(int i); //ajouter une valeur à m_valeurs

    inline void setInterpreteur(Interpreteur* inter) {
        this->m_interpreteur = inter;
    } //lien vers l'interpréteur.
private:
    NoeudInstProcedure* m_fonction;
    string m_signature;
    int m_nbArguments;
    vector<int> m_valeurs;
    Interpreteur* m_interpreteur;

};
#endif /* ARBREABSTRAIT_H */
