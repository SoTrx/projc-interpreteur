#ifndef SYMBOLEVALUE_H
#define SYMBOLEVALUE_H

#include <string.h>
#include <boost/variant.hpp>
#include <iostream>
using namespace std;

#include "Symbole.h"
#include "ArbreAbstrait.h"

class SymboleValue : public Symbole, // Un symbole valué est un symbole qui a une valeur (définie ou pas)
public Noeud { //  et c'est aussi une feuille de l'arbre abstrait
public:
    SymboleValue(const Symbole & s); // Construit un symbole valué à partir d'un symbole existant s

    ~SymboleValue() {
    }
    boost::variant<int, std::string> executer(); // exécute le SymboleValue (revoie sa valeur !)

    inline void setValeur(boost::variant<int, std::string> valeur) {
        this->m_valeur = valeur;
        m_defini = true;
    } // accesseur

    inline bool estDefini() {
        return m_defini;
    } // accesseur
    const bool estEntier() const; //Si la valeur du symbole est un entier.

    friend ostream & operator<<(ostream & cout, const SymboleValue & symbole); // affiche un symbole value sur cout
    void traduitEnCPP(ostream & out, unsigned int indentation) const;


private:
    bool m_defini; // indique si la valeur du symbole est définie
    boost::variant<int, std::string> m_valeur; // valeur du symbole si elle est définie, zéro sinon
    bool m_typeInt;

};

#endif /* SYMBOLEVALUE_H */
