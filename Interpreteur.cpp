#include "Interpreteur.h"
#include <stdlib.h>
#include <iostream>
using namespace std;


bool Interpreteur::m_exec = true;

Interpreteur::Interpreteur(ifstream & fichier) :
m_lecteur(fichier), m_table(), m_arbre(nullptr) {
}

void Interpreteur::analyse() {
    m_arbre = programme(); // on lance l'analyse de la première règle
}

void Interpreteur::tester(const string & symboleAttendu) const throw (SyntaxeException) {
    // Teste si le symbole courant est égal au symboleAttendu... Si non, lève une exception
    static char messageWhat[256];
    if (m_lecteur.getSymbole() != symboleAttendu) {
        sprintf(messageWhat,
                "Ligne %d, Colonne %d - Erreur de syntaxe - Symbole attendu : %s - Symbole trouvé : %s\n",
                m_lecteur.getLigne(), m_lecteur.getColonne(),
                symboleAttendu.c_str(), m_lecteur.getSymbole().getChaine().c_str());
        throw SyntaxeException(messageWhat);
    }
}

void Interpreteur::testerEtAvancer(const string & symboleAttendu) throw (SyntaxeException) {
    // Teste si le symbole courant est égal au symboleAttendu... Si oui, avance, Sinon, lève une exception
    tester(symboleAttendu);
    m_lecteur.avancer();
}

void Interpreteur::erreur(const string & message) const throw (SyntaxeException) {
    // Lève une exception contenant le message et le symbole courant trouvé
    // Utilisé lorsqu'il y a plusieurs symboles attendus possibles...
    static char messageWhat[256];
    sprintf(messageWhat,
            "Ligne %d, Colonne %d - Erreur de syntaxe - %s - Symbole trouvé : %s\n",
            m_lecteur.getLigne(), m_lecteur.getColonne(), message.c_str(), m_lecteur.getSymbole().getChaine().c_str());
    throw SyntaxeException(messageWhat);
}

Noeud* Interpreteur::programme() {
    // <programme> ::= procedure principale() <seqInst> finproc FIN_FICHIER
    bool fail;
    while (!fail) {
        try {
            instProcedure();
        } catch (SyntaxeException) {
            fail = true;
        }
    }
    testerEtAvancer("principale");
    testerEtAvancer("(");
    testerEtAvancer(")");
    Noeud* sequence = seqInst();
    testerEtAvancer("finproc");
    tester("<FINDEFICHIER>");
    return sequence;
}

Noeud* Interpreteur::seqInst() {
    // <seqInst> ::= <inst> { <inst> }
    NoeudSeqInst* sequence = new NoeudSeqInst();
    do {
        sequence->ajoute(inst());
    } while (m_lecteur.getSymbole() == "<VARIABLE>" || m_lecteur.getSymbole() == "si" || m_lecteur.getSymbole() == "tantque" || m_lecteur.getSymbole() == "repeter" || m_lecteur.getSymbole() == "pour" || m_lecteur.getSymbole() == "ecrire" || m_lecteur.getSymbole() == "lire" || m_lecteur.getSymbole() == "selon" || m_lecteur.getSymbole() == "appel");
    // Tant que le symbole courant est un début possible d'instruction...
    // Il faut compléter cette condition chaque fois qu'on rajoute une nouvelle instruction
    return sequence;
}

Noeud* Interpreteur::inst() {
    // <inst> ::= <affectation>  ; | <instSi>
    if (m_lecteur.getSymbole() == "<VARIABLE>") {
        Noeud *affect = affectation();
        testerEtAvancer(";");
        return affect;
    } else if (m_lecteur.getSymbole() == "si") {
        return instSi();
    } else if (m_lecteur.getSymbole() == "tantque") {
        return instTantQue();
    } else if (m_lecteur.getSymbole() == "repeter") {
        return instRepeter();
    } else if (m_lecteur.getSymbole() == "pour") {
        return instPour();
    } else if (m_lecteur.getSymbole() == "ecrire") {
        return instEcrire();
    } else if (m_lecteur.getSymbole() == "lire") {
        return instLire();
    } else if (m_lecteur.getSymbole() == "selon") {
        return instSelon();
    } else if (m_lecteur.getSymbole() == "appel") {
        return instAppel();
    }// Compléter les alternatives chaque fois qu'on rajoute une nouvelle instruction
    else erreur("Instruction incorrecte");
}

Noeud* Interpreteur::affectation() {
    // <affectation> ::= <variable> = <expression>
    Noeud* var = nullptr;
    Noeud* exp = nullptr;
    tester("<VARIABLE>");
    var = m_table.chercheAjoute(m_lecteur.getSymbole()); // La variable est ajoutée à la table eton la mémorise
    m_lecteur.avancer();
    if (m_lecteur.getSymbole() == "=") {
        m_lecteur.avancer();
        exp = expression(); // On mémorise l'expression trouvée
    } else if (m_lecteur.getSymbole() == "++" || m_lecteur.getSymbole() == "--") {
        try {

            exp = new NoeudOperateurBinaire(m_lecteur.getSymbole(), var, nullptr);
            m_lecteur.avancer();
        } catch (SyntaxeException se) {
            cout << se.what();
        }
    }
    return new NoeudAffectation(var, exp); // On renvoie un noeud affectation
}

Noeud* Interpreteur::expression() {
    // <expression> ::=<terme>{+<terme> |-<terme> }
    Noeud* term1 = terme();
    if (m_lecteur.getSymbole() == "+" || m_lecteur.getSymbole() == "-") {
        Symbole operateur = m_lecteur.getSymbole();
        m_lecteur.avancer();
        Noeud* term2 = terme();
        term1 = new NoeudOperateurBinaire(operateur, term1, term2);
    } else if (m_lecteur.getSymbole() == "++" || m_lecteur.getSymbole() == "--") {
        Symbole operateur = m_lecteur.getSymbole();
        m_lecteur.avancer();
        term1 = new NoeudOperateurBinaire(operateur, term1, nullptr);
    }
    return term1;
}

Noeud* Interpreteur::terme() {
    // <terme>::=<facteur> {*<facteur> |/<facteur> }
    Noeud* fact = facteur();
    if (m_lecteur.getSymbole() == "*" || m_lecteur.getSymbole() == "/") {
        Symbole operateur = m_lecteur.getSymbole();
        m_lecteur.avancer();
        Noeud* fact2 = terme();
        fact = new NoeudOperateurBinaire(operateur, fact, fact2);
    }
    return fact;
}

Noeud* Interpreteur::facteur() {
    // <facteur> ::=<entier> |<variable> |-<expBool> |non <expBool> |( <expBool> )
    Noeud* expBool1 = nullptr;
    if (m_lecteur.getSymbole() == "<VARIABLE>" || m_lecteur.getSymbole() == "<ENTIER>" || m_lecteur.getSymbole() == "<CHAINE>") {
        expBool1 = m_table.chercheAjoute(m_lecteur.getSymbole()); // on ajoute la variable ou l'entier à la table
        m_lecteur.avancer();
    } else if (m_lecteur.getSymbole() == "-") { // - <expBool>
        m_lecteur.avancer();
        expBool1 = new NoeudOperateurBinaire(Symbole("-"), m_table.chercheAjoute(Symbole("0")), expBool());
    } else if (m_lecteur.getSymbole() == "non") { // non <expBool>
        m_lecteur.avancer();
        expBool1 = new NoeudOperateurBinaire(Symbole("non"), expBool(), nullptr);
    } else if (m_lecteur.getSymbole() == "(") { // expression parenthésée
        m_lecteur.avancer();
        expBool1 = expBool();
        testerEtAvancer(")");
    } else {
        erreur("Facteur incorrect");
    }
    return expBool1;
}

Noeud* Interpreteur::expBool() {
    //    <expBool> ::= <relationET> {ou <relationEt> }
    Noeud* rEt = relationEt();
    if (m_lecteur.getSymbole() == "ou") {
        m_lecteur.avancer();
        rEt = new NoeudOperateurBinaire(Symbole("ou"), rEt, relationEt());
    }
    return rEt;
}

Noeud* Interpreteur::relationEt() {
    //<relationEt> ::=<relation> {et <relation> }
    Noeud* rel = relation();
    if (m_lecteur.getSymbole() == "et") {
        m_lecteur.avancer();
        rel = new NoeudOperateurBinaire(Symbole("et"), rel, relation());
    }
    return rel;
}

Noeud* Interpreteur::relation() {
    //  <relation> ::= <expression> {<opRel> <expression> }
    //  <opRel> ::= ==|!=| < | <= | > | >
    Noeud* expr = expression();
    if (m_lecteur.getSymbole() == "==" ||
            m_lecteur.getSymbole() == "!=" ||
            m_lecteur.getSymbole() == "<" ||
            m_lecteur.getSymbole() == "<=" ||
            m_lecteur.getSymbole() == ">" ||
            m_lecteur.getSymbole() == ">=") {
        Symbole operateur = m_lecteur.getSymbole();
        m_lecteur.avancer();
        expr = new NoeudOperateurBinaire(operateur, expr, expression());
    }
    return expr;
}

Noeud* Interpreteur::instSi() {
    // <instSi> ::= si ( <expression> ) <seqInst> finsi
    try {
        testerEtAvancer("si");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
    }
    try {
        testerEtAvancer("(");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
    }


    Noeud* condition = expBool(); // On mémorise la condition
    try {
        testerEtAvancer(")");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
    }

    Noeud* sequence = seqInst(); // On mémorise la séquence d'instruction
    vector<Noeud*> noeudsSinonsi;
    bool fail = false;
    int i = 0;
    while (!fail) {
        try {
            ++i;
            testerEtAvancer("sinonsi");

            try {
                testerEtAvancer("(");
            } catch (SyntaxeException se) {
                cout << se.what();
                m_exec = false;
            }


            noeudsSinonsi.push_back(expBool()); // On mémorise la condition

            try {
                testerEtAvancer(")");
            } catch (SyntaxeException se) {
                cout << se.what();
                m_exec = false;
            }

            noeudsSinonsi.push_back(seqInst());


        } catch (SyntaxeException se) {
            if (noeudsSinonsi.size() % 2 == 1) {
                cout << "Erreur : " << m_lecteur.getSymbole() << " colonne " << m_lecteur.getColonne() << " ligne " << m_lecteur.getLigne();
                throw;

            } else {
                fail = true;
            }

        }
    }
    Noeud * sequenceSinon = nullptr;
    try {
        testerEtAvancer("sinon");
        sequenceSinon = seqInst();
    } catch (SyntaxeException se) {
    }

    try {
        testerEtAvancer("finsi");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
    }

    return new NoeudInstSi(condition, sequence, noeudsSinonsi, sequenceSinon); // Et on renvoie un noeud Instruction Si
}

Noeud* Interpreteur::instTantQue() {
    //<intTantQue> ::= tantque( <expression> ) <seqInst> fintantque
    Noeud* condition = nullptr;
    Noeud* sequence = nullptr;
    string inst[] = {"tantque", "(", "cond", ")", "seq", "fintantque"}; // On place les symbole que l'on vas tester dans un tableau    
    // et si le symbole en cours est égal à cond ou à seq 
    // on execute des méthodes spécifiques sinon on testeEtAvance
    // Cela rend le code plus simple à lire
    for (string s : inst) {
        try {
            if (s == "cond") {
                condition = expBool();
            } else if (s == "seq") {
                sequence = seqInst();
            } else {
                testerEtAvancer(s);
            }
        } catch (SyntaxeException se) {
            cout << se.what();
            m_exec = false;


        }
    }

    return new NoeudInstTantQue(condition, sequence);

}

Noeud* Interpreteur::instRepeter() {
    //<instRepeter ::= repeter <seqInst> jusqua ( <expression> )
    Noeud* condition = nullptr;
    Noeud* sequence = nullptr;
    string inst[] = {"repeter", "seq", "jusqua", "(", "cond", ")"}; // On place les symbole que l'on vas tester dans un tableau
    for (string s : inst) { // et si le symbole en cours est egal à cond ou à seq 
        try { // on execute des méthodes spécifiques. Sinon on teste et avance
            if (s == "cond") { // Cela rend le code plus simple à lire
                condition = expBool();
            } else if (s == "seq") {
                sequence = seqInst();
            } else {
                testerEtAvancer(s);
            }
        } catch (SyntaxeException se) {
            cout << se.what();
            m_exec = false;
            m_lecteur.avancer();

        }
    }
    return new NoeudInstRepeter(condition, sequence);

}

Noeud* Interpreteur::instPour() {
    //<instPour> ::= pour ( [ <affectation> ] ; <expression> ; [ <affectation> ] ) <seqInst> finpour
    Noeud* condition = nullptr;
    Noeud* sequence = nullptr;
    Noeud* affectation1 = nullptr;
    Noeud* affectation2 = nullptr;

    string inst[] = {"pour", "(", "aff", ";", "cond", ";", "aff2", ")", "seq", "finpour"}; // On place les symbole que l'on vas tester dans un tableau
    for (string s : inst) { // et si le symbole en cours est égal à cond ou aff eou seq
        try { // on execute des méthodes spécifiques à celle-ci sinon on test et avance
            if (s == "cond") {
                condition = expBool();
            } else if (s == "seq") {
                sequence = seqInst();
            } else if (s == "aff") {
                try {
                    affectation1 = affectation(); // On mémorise la séquence d'instruction

                } catch (SyntaxeException se) {
                }
            } else if (s == "aff2") {
                try {
                    affectation2 = affectation(); // On mémorise la séquence d'instruction
                } catch (SyntaxeException se) {
                }
            } else {
                testerEtAvancer(s);
            }
        } catch (SyntaxeException se) {

            cout << se.what();
            m_exec = false;
            m_lecteur.avancer();

        }
    }
    return new NoeudInstPour(affectation1, condition, affectation2, sequence);
}

Noeud* Interpreteur::instEcrire() {
    // <instEcrire> ::= ecrire(<expression> |<chaine> {,<expression> | <chaine> })
    NoeudInstEcrire* instE = new NoeudInstEcrire();
    try {
        testerEtAvancer("ecrire");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
        m_lecteur.avancer();
    }
    tester("(");
    try {
        do {
            m_lecteur.avancer();
            if (m_lecteur.getSymbole() == "<CHAINE>") { // Dans le cas où le symbole courant n'est pas une chaine ou une variable on lance une SyntaxeException
                instE->ajoute(new SymboleValue(m_lecteur.getSymbole()));
                m_lecteur.avancer();
            } else if (m_lecteur.getSymbole() == "<VARIABLE>") {
                instE->ajoute(expression());
            } else {
                cout << m_lecteur.getSymbole();
                throw new SyntaxeException();
            }
        } while (m_lecteur.getSymbole() == ","); //Tant qu'il y a une virgule ont continue de regarder si il y a une chaine ou une variable
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
        m_lecteur.avancer();
    }

    try {
        testerEtAvancer(")");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
        m_lecteur.avancer();
    }
    try {
        testerEtAvancer(";");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;

    }


    return instE;
}

Noeud* Interpreteur::instLire() {
    // <instLire> ::= lire( <variable> {,<variable> })
    NoeudInstLire* instL = new NoeudInstLire();
    try {
        testerEtAvancer("lire");
        tester("(");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
    }
    try {
        do {
            m_lecteur.avancer();
            tester("<VARIABLE>");
            Noeud* var = m_table.chercheAjoute(m_lecteur.getSymbole());
            instL->ajoute(var); //On ajoute l'instruction 
            m_lecteur.avancer();
        } while (m_lecteur.getSymbole() == ",");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
    }

    try {
        testerEtAvancer(")");
        testerEtAvancer(";");
    } catch (SyntaxeException se) {
        cout << se.what();
        m_exec = false;
    }
    return instL;

}

Noeud* Interpreteur::instSelon() {
    // <instSelon> ::= selon(<variable>) {cas <Entier> : <seqInst>} defaut : <seqInst> finselon
    vector<Noeud*> instCas;
    testerEtAvancer("selon");
    testerEtAvancer("(");
    tester("<VARIABLE>");
    Noeud* var = m_table.chercheAjoute(m_lecteur.getSymbole());
    m_lecteur.avancer();
    testerEtAvancer(")");
    while (m_lecteur.getSymbole() == "cas") { //Tant que l'on a le symbole cas, on test si il est suivie d'un entier et de ":"
        m_lecteur.avancer();
        tester("<ENTIER>");
        SymboleValue* var2 = m_table.chercheAjoute(m_lecteur.getSymbole()); // on ajoute la valeur à la table des signes
        instCas.push_back(var2);
        m_lecteur.avancer();
        testerEtAvancer(":");
        Noeud * seq = seqInst();
        instCas.push_back(seq);
    }
    testerEtAvancer("defaut");
    testerEtAvancer(":");
    Noeud* seqInstDefaut = seqInst();
    testerEtAvancer("finselon");

    return new NoeudInstSelon(var, seqInstDefaut, instCas);


}

Noeud* Interpreteur::instProcedure() {
    string signature;
    NoeudInstProcedure* proc = new NoeudInstProcedure();
    testerEtAvancer("procedure");
    tester("<VARIABLE>");
    signature += m_lecteur.getSymbole().getChaine(); //ajout nom de la procedure [ p1 ]
    m_lecteur.avancer();
    testerEtAvancer("(");
    signature += "("; // ajout parenthèse [ p1( ] 
    bool first = true;
    if (m_lecteur.getSymbole() != ")") {
        do {
            if (!first) {
                m_lecteur.avancer();
                signature += ",";
            }
            first = false;
            tester("<VARIABLE>");
            proc->setNbArgs(proc->getNbArgs() + 1);
            signature += m_lecteur.getSymbole().getChaine(); // ajout parenthèse [ p1(var,... ] 
            m_lecteur.avancer();
        } while (m_lecteur.getSymbole() == ",");
    }
    testerEtAvancer(")"); //ajout parenthèse [p1(vars)]
    signature += ")";
    proc->setSignature(signature);
    proc->ajoute(seqInst());
    testerEtAvancer("finproc");

    NoeudInstProcedure::ajouter(signature, proc);
    this->m_procedures.push_back(proc);

}

Noeud* Interpreteur::instAppel() {
    NoeudInstAppel* app = new NoeudInstAppel();
    app->setInterpreteur(this); //lien vers l'interpréteur sauvegardé pour modification en cours d'exécution de sa table
    testerEtAvancer("appel");
    string signature; // La signature de la fonction appelée.
    tester("<VARIABLE>");
    signature += m_lecteur.getSymbole().getChaine();
    m_lecteur.avancer();
    tester("(");
    signature += m_lecteur.getSymbole().getChaine();
    m_lecteur.avancer();
    bool first = true;
    if (m_lecteur.getSymbole() != ")") { // Si il n'y a pas 0 arguments.
        do {
            if (!first) { //Si ce n'est pas la première fois que l'on passe dans la boucle, on saute un caractère(la virgule)
                signature += m_lecteur.getSymbole().getChaine();
                m_lecteur.avancer();
            }
            first = false;
            tester("<ENTIER>");
            app->ajouter(stoi(m_lecteur.getSymbole().getChaine()));
            signature += m_lecteur.getSymbole().getChaine();
            m_lecteur.avancer();
        } while (m_lecteur.getSymbole() == ",");
    }
    tester(")");
    signature += m_lecteur.getSymbole().getChaine(); //Signature totalement reconstituée.
    m_lecteur.avancer();
    app->cherche(signature); // on va chercher la fonction correspondant à la signature.
    testerEtAvancer(";");
    return app;
}

void Interpreteur::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << "#include<iostream>" << endl;
    out << "#include<string>" << endl;
    out << "using namespace std;" << endl << endl;
    for (Noeud* node : m_procedures) {
        node->traduitEnCPP(out, 0);
    }
    out << "" << "int main() {" << endl;
    // Début d’un programme C++
    // Ecrire en C++ la déclaration des variables présentes dans le programme... 
    // ... variables dont on retrouvera le nom en parcourant la table des symboles ! 
    // Par exemple, si le programme contient i,j,k, il faudra écrire : ? i; ? j; ? k; ... 
    for (int i = 0; i<this->m_table.getTaille(); i++) {
        if (this->m_table[i] == "<VARIABLE>") {
            if (this->m_table[i].estEntier()) { //On détermine le type des variables.
                out << setw(4 * (indentation + 2)) << "int " << this->m_table[i].getChaine() << ";" << endl;
            } else {
                out << setw(4 * (indentation + 2)) << "string " << this->m_table[i].getChaine() << ";" << endl;
            }

        }
    }
    getArbre()->traduitEnCPP(out, indentation + 1);
    // lance l'opération traduitEnCPP sur la racine
    out << setw(0) << "" << "return 0;" << endl;
    out << setw(0) << "}" << endl;
    // Fin d’un programme C++
}

