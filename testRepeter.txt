#Test du "repeter"
#Resultats attendus :
#Test 1 : "Vous pouvez répéter la question ?" écrit 3 fois.
#Test 2 : "Stéphanie de Monaco" écrit 4 fois.

procedure principale()
    #Test 1 : avec une condition testant l'égalité.
    ecrire("Test 1 égalité");
    q = 0;
    repeter
       ecrire("Vous pouvez répéter la question ?");
       q = q+1;
    jusqua(q == 3)
    ecrire(" ------------------------------");
    ecrire("");

    #Test 2 : avec une inéquation;
    ecrire("Test 2 : avec une inéquation.");
    q2 = 0;
    repeter
       ecrire("Stéphanie de Monaco");
       q2 = q2+1;
    jusqua(q2 > 3)
    ecrire(" ------------------------------");
    ecrire("");

finproc