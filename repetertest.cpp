/*
 * File:   repetertest.cpp
 * Author: lucas
 *
 * Created on 17 oct. 2015, 13:38:08
 */

#include "repetertest.h"
#include "ArbreAbstrait.h"
#include "Interpreteur.h"
using namespace std;

CPPUNIT_TEST_SUITE_REGISTRATION(repetertest);

repetertest::repetertest() {
}

repetertest::~repetertest() {
}

void repetertest::setUp() {
}

void repetertest::tearDown() {
}

void repetertest::testNoeudInstRepeter() {
    string nomFich = "testRepeter.txt";
    ifstream fichier(nomFich.c_str());
    try {
        Interpreteur interpreteur(fichier);
        interpreteur.analyse();
        if (interpreteur.getArbre() != nullptr) {          
            interpreteur.getArbre()->executer();
        }
         int q;
        for(int i=0;i<interpreteur.getTable().getTaille();i++){
            if(interpreteur.getTable()[i].getChaine() == "q"){
                CPPUNIT_ASSERT_EQUAL_MESSAGE("q n'est pas égal à sa valeur attendue(3) après le premier test",3,boost::get<int>(((SymboleValue) interpreteur.getTable()[i]).executer()));
            }else if(interpreteur.getTable()[i].getChaine() == "q2"){
                CPPUNIT_ASSERT_EQUAL_MESSAGE("q2 n'est pas égal à sa valeur attendue(4) après le second test",4,boost::get<int>(((SymboleValue) interpreteur.getTable()[i]).executer()));
            }
        }
        cout << q;
    } catch (InterpreteurException & e) {
        cout << e.what() << endl;
    }
    
}
void repetertest::testFautesSyntaxe(){
        string nomFich = "testRepeterFauteSyntaxe.txt";
        ifstream fichier(nomFich.c_str());
        Interpreteur interpreteur(fichier);
        CPPUNIT_ASSERT_THROW_MESSAGE("Exception non levée",interpreteur.analyse(),SyntaxeException);
        
 }


