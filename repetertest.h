/*
 * File:   repetertest.h
 * Author: lucas
 *
 * Created on 17 oct. 2015, 13:38:08
 */

#ifndef REPETERTEST_H
#define	REPETERTEST_H

#include <cppunit/extensions/HelperMacros.h>

class repetertest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(repetertest);

    CPPUNIT_TEST(testNoeudInstRepeter);
    CPPUNIT_TEST(testFautesSyntaxe);

    CPPUNIT_TEST_SUITE_END();

public:
    repetertest();
    virtual ~repetertest();
    void setUp();
    void tearDown();

private:
    void testNoeudInstRepeter();
    void testFautesSyntaxe();

};

#endif	/* REPETERTEST_H */

