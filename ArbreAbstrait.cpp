#include <stdlib.h>
#include <typeinfo>
#include "ArbreAbstrait.h"
#include "Symbole.h"
#include "SymboleValue.h"
#include "Exceptions.h"
#include "Interpreteur.h"
#include <limits>
#include <sstream>
#include <boost/variant/variant.hpp>
#include <boost/variant/get.hpp>

////////////////////////////////////////////////////////////////////////////////
// NoeudSeqInst
////////////////////////////////////////////////////////////////////////////////

NoeudSeqInst::NoeudSeqInst() : m_instructions() {
}

boost::variant<int, std::string> NoeudSeqInst::executer() {
    for (unsigned int i = 0; i < m_instructions.size(); i++) {

        Noeud* temp = m_instructions[i];
        temp->executer(); // on exécute chaque instruction de la séquence
    }
    return 0; // La valeur renvoyée ne représente rien !
}

void NoeudSeqInst::ajoute(Noeud* instruction) {
    if (instruction != nullptr) m_instructions.push_back(instruction);
}

void NoeudSeqInst::traduitEnCPP(ostream & out, unsigned int indentation)const {
    for (unsigned int i = 0; i < m_instructions.size(); i++) {
        m_instructions[i]->traduitEnCPP(out, indentation); // on exécute chaque instruction de la séquence
        out << endl;
        // Fin d’un programme C++
    }
}
////////////////////////////////////////////////////////////////////////////////
// NoeudAffectation
////////////////////////////////////////////////////////////////////////////////

NoeudAffectation::NoeudAffectation(Noeud* variable, Noeud* expression)
: m_variable(variable), m_expression(expression) {
}

boost::variant<int, std::string> NoeudAffectation::executer() {
    boost::variant<int, std::string> valeur = m_expression->executer(); // On exécute (évalue) l'expression
    ((SymboleValue*) m_variable)->setValeur(valeur); // On affecte la variable
    return 0; // La valeur renvoyée ne représente rien !
}

void NoeudAffectation::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << setw((4 * (indentation + 1) - 1));
    m_variable->traduitEnCPP(out, 0);
    out << "=";
    m_expression->traduitEnCPP(out, 0); // On exécute (évalue) l'expression
    out << ";";
}
////////////////////////////////////////////////////////////////////////////////
// NoeudOperateurBinaire
////////////////////////////////////////////////////////////////////////////////

NoeudOperateurBinaire::NoeudOperateurBinaire(Symbole operateur, Noeud* operandeGauche, Noeud* operandeDroit)
: m_operateur(operateur), m_operandeGauche(operandeGauche), m_operandeDroit(operandeDroit) {
}

boost::variant<int, std::string> NoeudOperateurBinaire::executer() {
    boost::variant<int, std::string> ogB, odB, valeurB;
    if (m_operandeGauche != nullptr) ogB = m_operandeGauche->executer(); // On évalue l'opérande gauche
    if (m_operandeDroit != nullptr) odB = m_operandeDroit->executer(); // On évalue l'opérande droit
    // Et on combine les deux opérandes en fonctions de l'opérateur
    if (odB.type() == typeid (int) && ogB.type() == typeid (int)) { //Détermination du type des operandes. Si ce sont des entier.
        int og = boost::get<int>(ogB); //Nouvelles variables ayant un type "fixe"
        int od = boost::get<int>(odB);
        int valeur;
        if (this->m_operateur == "+") valeur = (og + od);
        else if (this->m_operateur == "-") valeur = (og - od);
        else if (this->m_operateur == "*") valeur = (og * od);
        else if (this->m_operateur == "==") valeur = (og == od);
        else if (this->m_operateur == "!=") valeur = (og != od);
        else if (this->m_operateur == "<") valeur = (og < od);
        else if (this->m_operateur == ">") valeur = (og > od);
        else if (this->m_operateur == "<=") valeur = (og <= od);
        else if (this->m_operateur == ">=") valeur = (og >= od);
        else if (this->m_operateur == "et") valeur = (og && od);
        else if (this->m_operateur == "ou") valeur = (og || od);
        else if (this->m_operateur == "non") valeur = (!og);
        else if (this->m_operateur == "++") valeur = og + 1;
        else if (this->m_operateur == "--") valeur = og - 1;
        else if (this->m_operateur == "/") {
            if (od == 0) throw DivParZeroException();
            valeur = og / od;
        }
        return valeur;
    } else if (odB.type() == typeid (std::string) && ogB.type() == typeid (std::string)) { //Sinon, ici, les opérandes sont des strings
        string og = boost::get<string>(ogB); //Variables "de travail"
        string od = boost::get<string>(odB);
        string valeur;
        if (this->m_operateur == "+") valeur = (og + od);
        return valeur; // On retourne la valeur calculée
    }

}

void NoeudOperateurBinaire::traduitEnCPP(ostream & out, unsigned int indentation)const {

    m_operandeGauche->traduitEnCPP(out, 0);
    out << m_operateur.getChaine(); //L'opérateur binaire est placée entre les deux opérande
    m_operandeDroit->traduitEnCPP(out, 0);
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstSi
////////////////////////////////////////////////////////////////////////////////

NoeudInstSi::NoeudInstSi(Noeud* condition, Noeud* sequence, vector<Noeud*> noeudsSinonsi, Noeud* sequenceSinon)
: m_conditionSi(condition), m_sequenceSi(sequence), m_v_sinonsi(noeudsSinonsi), m_sequenceSinon(sequenceSinon) {
}

boost::variant<int, std::string> NoeudInstSi::executer() {
    int i = 0;
    do {
        if (boost::get<int>(m_conditionSi->executer()) && i == 0) { // Si la condition du "si" est vérifiée.
            m_sequenceSi->executer(); // On execute les operations associées.
        } else if (i == m_v_sinonsi.size()) { //Si i a la même taille que le vecteur des sinonsi, alors ils sont tous passés. On peut donc executer le sinon.
            m_sequenceSinon->executer(); // Operations du Sinon.
        } else if (!m_v_sinonsi.empty() && boost::get<int>(m_v_sinonsi[i]->executer())) { //On teste les sinonsi un par un. ([i] pair = condition)
            m_v_sinonsi[i + 1]->executer(); //operation des sinonsi
        }
        i += 2;
    } while (i <= m_v_sinonsi.size());

    return 0; // La valeur renvoyée ne représente rien !
}

void NoeudInstSi::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << setw(4 * indentation) << "" << "if ("; // Ecrit "if (" avec un décalage de 4*indentation espaces 
    m_conditionSi->traduitEnCPP(out, 0); // Traduit la condition en C++ sans décalage 
    out << ") {" << endl; // Ecrit ") {" et passe à la ligne 
    m_sequenceSi->traduitEnCPP(out, indentation + 1); // Traduit en C++ la séquence avec indentation augmentée 
    out << setw(4 * indentation) << "" << "}"; // Ecrit "}" avec l'indentation initiale et passe à la ligne 
    if (!m_v_sinonsi.empty()) {
        for (int i = 0; i < m_v_sinonsi.size(); i += 2) {
            out << setw(0) << "else if ("; // Ecrit un else if (
            m_v_sinonsi[i]->traduitEnCPP(out, 0); // On ecrit la condition du elsei f
            out << ") {" << endl;
            m_v_sinonsi[i + 1]->traduitEnCPP(out, indentation + 1); // On écrit a séquence d'instruction du else if
            out << setw(4 * indentation) << "" << "}"; // Ecrit "}" avec l'indentation initiale et passe à la ligne 
        }
    }
    if (this->m_sequenceSinon != nullptr) { // On écrit else { dans le cas où il y en a un
        out << setw(0) << "else {" << endl;
        m_sequenceSinon->traduitEnCPP(out, indentation + 1); // Traduit en C++ la séquence avec indentation augmentée 
        out << setw(4 * indentation) << "" << "}"; // Ecrit "}" avec l'indentation initiale et passe à la ligne 
    }
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstTantQue
////////////////////////////////////////////////////////////////////////////////

NoeudInstTantQue::NoeudInstTantQue(Noeud* condition, Noeud* sequence)
: m_condition(condition), m_sequence(sequence) {
}

boost::variant<int, std::string> NoeudInstTantQue::executer() {
    while (boost::get<int>(m_condition->executer())) m_sequence->executer(); //On execute tant que la condition est vérifiée.
    return 0; // La valeur renvoyée ne représente rien !
}

void NoeudInstTantQue::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << setw(4 * indentation) << "" << "while ("; // Ecrit "while (" avec un décalage de 4*indentation espaces 
    m_condition->traduitEnCPP(out, 0); // Traduit la condition en C++ sans décalage 
    out << ") {" << endl; // Ecrit ") {" et passe à la ligne 
    m_sequence->traduitEnCPP(out, indentation + 1); // Traduit en C++ la séquence avec indentation augmentée 
    out << setw(4 * indentation) << "}"; // Ecrit "}" avec l'indentation initiale et passe à la ligne 
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstRepeter
////////////////////////////////////////////////////////////////////////////////

NoeudInstRepeter::NoeudInstRepeter(Noeud* condition, Noeud* sequence)
: m_condition(condition), m_sequence(sequence) {
}

boost::variant<int, std::string> NoeudInstRepeter::executer() {
    do {
        m_sequence->executer();
    } while (!(boost::get<int>(m_condition->executer()))); //jusqua est l'inverse d'un while. On inverse donc la condition de boucle.
    return 0; // La valeur renvoyée ne représente rien !
}

void NoeudInstRepeter::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << setw(4 * indentation) << "" << "do {" << endl; // Ecrit "do {" avec un décalage de 4*indentation espaces 
    m_sequence->traduitEnCPP(out, indentation + 1); // Traduit en C++ la séquence avec indentation augmentée

    out << setw(10 * indentation - 1) << "} while (!("; // Ecrit "} while (!(" et passe à la ligne 
    m_condition->traduitEnCPP(out, indentation); // Traduit la condition en C++ sans décalage 

    out << "));"; // Ecrit "));" avec l'indentation initiale et passe à la ligne 
}

////////////////////////////////////////////////////////////////////////////////
// NoeudInstPour
////////////////////////////////////////////////////////////////////////////////

NoeudInstPour::NoeudInstPour(Noeud* affectation, Noeud* condition, Noeud* affectation2, Noeud* sequence)
: m_affectation(affectation), m_condition(condition), m_affectation2(affectation2), m_sequence(sequence) {
}

boost::variant<int, std::string> NoeudInstPour::executer() {
    /*
     Tests ternaires utilisés pour rendre le nombre d'argument du pour modulable.
     */
    for (((m_affectation != nullptr) ? (boost::get<int>(m_affectation->executer())) : 0); (boost::get<int>(m_condition->executer())); ((m_affectation2 != nullptr) ? (boost::get<int>(m_affectation2->executer())) : 0)) {
        m_sequence->executer();
    }
    return 0; // La valeur renvoyée ne représente rien !
}

void NoeudInstPour::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << setw(4 * indentation) << "" << "for ("; // Ecrit "for (" avec un décalage de 4*indentation espaces 
    if (m_affectation != nullptr) { // On test si il y a une affectation, si oui on l'écrit sinon on laisse l'éspace vide
        m_affectation->traduitEnCPP(out, 0);
    } else {
        out << ";";
    } // Traduit la condition en C++ sans décalage 
    m_condition->traduitEnCPP(out, 0);
    out << ";"; // Ecrit ";"
    if (m_affectation2 != nullptr) { // On test si il y a une affectation, si oui on l'écrit sinon on laisse l'éspace vide
        stringstream temp;
        m_affectation2->traduitEnCPP(temp, 0);
        string tempstr = temp.str();
        out << tempstr.substr(0, tempstr.length() - 1);

    } // Traduit la condition en C++ sans décalage
    out << ") {" << endl;
    m_sequence->traduitEnCPP(out, indentation + 1); // Traduit en C++ la séquence avec indentation augmentée 
    out << setw(4 * indentation) << "" << "}"; // Ecrit "}" avec l'indentation initiale
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstEcrire
////////////////////////////////////////////////////////////////////////////////

NoeudInstEcrire::NoeudInstEcrire() {
}

boost::variant<int, std::string> NoeudInstEcrire::executer() {
    for (Noeud* p : m_v_instructions) {
        if ((typeid (*p) == typeid (SymboleValue) && *((SymboleValue*) p) == "<CHAINE>")) { // Si le symbole est une chaîne.
            string temp = (*((SymboleValue*) p)).getChaine();
            cout << temp.substr(1, temp.size() - 2); // On enlève à la chaîne ses guillemets
        } else {

            cout << p->executer(); //Sinon on affiche la valeur du Symbole.
        }
    }
    cout << endl;
    return 0; // La valeur renvoyée ne représente rien !*/
}

void NoeudInstEcrire::ajoute(Noeud* instruction) {
    if (instruction != nullptr) {
        m_v_instructions.push_back(instruction);
    }
}

void NoeudInstEcrire::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << setw(4 * indentation) << "" << "cout ";
    for (Noeud* p : m_v_instructions) {//Pour chaque instruction, si c'est une chaine alors on l'écrit sinon on traduit la variable en cpp
        out << "<< ";
        if ((typeid (*p) == typeid (SymboleValue) && *((SymboleValue*) p) == "<CHAINE>")) {
            out << (*((SymboleValue*) p)).getChaine();
        } else {
            p->traduitEnCPP(out, indentation + 1);
        }
    }
    out << " << endl;";
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstLire
////////////////////////////////////////////////////////////////////////////////

NoeudInstLire::NoeudInstLire() {
}
namespace boost {

    std::istream& operator>>(std::istream& in, variant<int, std::string>& v) //Redéfinition du >> pour le variant<int,string>
    {
        in >> std::ws;
        int c = in.peek();
        if (c == EOF) return in;
        if (std::isdigit(static_cast<unsigned char> (c)) || c == '-') { //Si l'utilisateur rentre un Entier, alors variant stocke un entier
            int i;
            in >> i;
            v = i;
        } else { //Sinon il stocke une chaîne.
            std::string s;
            in >> s;
            v = s;
        }
        return in;
    }
} // namespace boost

boost::variant<int, std::string> NoeudInstLire::executer() {
    for (Noeud* var : m_v_variables) {
        boost::variant<int, std::string> temp = 0;
        do {
            if (!cin.good()) {//dans le cas où la var n'est pas correcte pour cin, on fait en sorte que cin l'ignore
                cin.clear();
                cin.ignore(std::numeric_limits<streamsize>::max(), '\n');
            }
            cin >> temp;
        } while (!cin.good());
        ((SymboleValue*) var)->setValeur(temp);
    }
    return 0; // La valeur renvoyée ne représente rien !*/
}

void NoeudInstLire::ajoute(Noeud* variable) {
    if (variable != nullptr) {
        m_v_variables.push_back(variable);
    }
}

void NoeudInstLire::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << setw(4 * indentation) << "" << "cin"; // On écrit cin puis pour chaque variable on va écrire ">>" suivi de la variable
    for (Noeud* var : m_v_variables) {
        out << " >>";
        ((SymboleValue*) var)->traduitEnCPP(out, indentation);
    }
    out << ";";
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstSelon
////////////////////////////////////////////////////////////////////////////////

NoeudInstSelon::NoeudInstSelon(Noeud* variable, Noeud* instructionsDefaut, vector<Noeud*>& instructionsCas) : m_variable(variable), m_v_instructionsCas(instructionsCas), m_instructionsDefaut(instructionsDefaut) {
}

boost::variant<int, std::string> NoeudInstSelon::executer() {
    bool exec = false;
    for (int i = 0; i < m_v_instructionsCas.size(); i += 2) {//pour chaque cas on regarde si la variable du selon est égale à la valeur du cas
        if (m_variable->executer() == m_v_instructionsCas[i]->executer()) {
            exec = true;
            m_v_instructionsCas[i + 1]->executer(); // On execute la séquence d'instruction du cas (i pair: valeur cas, i impair: séquence d'instruction du cas)
        }
    }
    if (!exec) {
        m_instructionsDefaut->executer();
    }
    return 0; // La valeur renvoyée ne représente rien !*/
}

void NoeudInstSelon::traduitEnCPP(ostream & out, unsigned int indentation)const {
    out << setw((8 * indentation) - 2) << "switch("; //On écrit switch(<variable>) {
    m_variable->traduitEnCPP(out, 0);
    out << ") {" << endl;
    for (int i = 0; i < m_v_instructionsCas.size(); i += 2) {// Pour chaque instruction on écrit case <valeur> : la séquence d'instruction suivie d'un break
        out << setw(6 * (indentation + 1)) << "case ";
        ((SymboleValue*) m_v_instructionsCas[i])->traduitEnCPP(out, 0);
        out << " : ";
        m_v_instructionsCas[i + 1]->traduitEnCPP(out, 0);
        out << setw((6 * (indentation + 1)) + 4) << " break;" << endl;
    }
    out << setw((8 * (indentation + 1) - 1)) << "default : "; // dans le cas où il y a un defaut on écrit default
    m_instructionsDefaut->traduitEnCPP(out, 0);
    out << setw(4 * indentation) << "}";
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstProcedure
////////////////////////////////////////////////////////////////////////////////
map<string, NoeudInstProcedure*> NoeudInstProcedure::m_procedures;

NoeudInstProcedure::NoeudInstProcedure() {
}

boost::variant<int, std::string> NoeudInstProcedure::executer() {
    for (Noeud* instr : m_v_instructions) {
        instr->executer();
    }
    return 0; // La valeur renvoyée ne représente rien !*/
}

void NoeudInstProcedure::traduitEnCPP(ostream & out, unsigned int indentation)const {
    string strProc = this->m_signature;
    for (int i = 1; i <= (this->m_nbArgs * 6); i += 6) { //Modification de la signature de la fonction pour pouvoir donner un type aux arguments.
        strProc.insert(strProc.find("(") + i, "int ");
    }

    out << setw(4 * indentation) << "void " << strProc << "{" << endl;
    for (Noeud* node : m_v_instructions) {
        node->traduitEnCPP(out, indentation + 1);
    }
    out << "}" << endl;
}

void NoeudInstProcedure::ajouter(string signature, NoeudInstProcedure* procedure) {
    m_procedures.insert(pair<string, NoeudInstProcedure*>(signature, procedure));
}

void NoeudInstProcedure::ajoute(Noeud* instruction) {
    this->m_v_instructions.push_back(instruction);
}
////////////////////////////////////////////////////////////////////////////////
// NoeudInstAppel
////////////////////////////////////////////////////////////////////////////////

NoeudInstAppel::NoeudInstAppel() {
}

boost::variant<int, std::string> NoeudInstAppel::executer() {
    vector<Symbole*> symboles;
    vector<SymboleValue> sav;

    /* 
     Identification des symboles 
     */
    for (int i = 1; i <= (this->m_nbArguments * 2); i += 2) {
        symboles.push_back(new Symbole(m_signature.substr((this->m_signature.find("(") + i), 1)));
    }
    /*
     * On recherche les symboles dans la table de valeur de l'interpréteur. Si les symboles correspondants aux variables de la fonction sont déjà présentes, alors
     * on les sauvegarde. Sinon, on crée de nouvelles entrées dans la table à titre temporaire.
     */
    for (int i = 0; i < symboles.size(); i++) {
        Symbole op = (*symboles[i]);
        Interpreteur* innte = m_interpreteur;
        SymboleValue* sV = this->m_interpreteur->getAccesTable().cherche(op);
        if (sV == nullptr) {
            sV = this->m_interpreteur->getAccesTable().chercheAjoute((*symboles[i]));
        } else {
            sav.push_back(*sV);
        }
        sV->setValeur(this->m_valeurs[i]); //Valeurs
    }
    /*
     Execution de la fonction appelée.
     */
    this->m_fonction->executer();

    /*
     Restauration des valeurs des variables présentes avant l'appel de la fonction. Suppresion des variables crées pour la fonction.
     */
    for (int i = 0; i < sav.size(); i++) {
        SymboleValue* sV = this->m_interpreteur->getAccesTable().cherche(sav[i]);
        try {
            sV->setValeur(sav[i].executer());
        } catch (IndefiniException ie) {
            this->m_interpreteur->getAccesTable().chercheSupprime(sav[i]);
        }
    }


    return 0; // La valeur renvoyée ne représente rien !*/
}

void NoeudInstAppel::traduitEnCPP(ostream & out, unsigned int indentation)const {
    string strApp = this->m_signature;
    int j = 0;
    for (int i = 1; i <= (this->m_nbArguments * 2); i += 2) {
        strApp.replace(strApp.find("(") + i, 1, to_string(this->m_valeurs[j]));
        j++;
    }
    out << setw(4 * (indentation + 1) + 3) << strApp << ";";
}

void NoeudInstAppel::ajouter(int i) {
    this->m_valeurs.push_back(i);
}

void NoeudInstAppel::cherche(string& modele) {
    /*
     * Cherche la fonction correspondante à l'appel on comparant les signatures de l'appel et de celles stockées.
     */
    map<string, NoeudInstProcedure*> m = NoeudInstProcedure::getProcs();
    string name = modele.substr(0, modele.find("(")); //Extraction du nom du modèle
    const char * modeleC = modele.c_str();
    int occurencesMod = 0, occurencesM;

    for (auto c : modele) { // Nombre de virgule compté pour le modèle.
        if (c == ',') {
            occurencesMod++;
        }
    }

    for (map<string, NoeudInstProcedure*>::iterator it = m.begin(); it != m.end(); it++) {
        string sm = it->first;
        string nameM = sm.substr(0, sm.find("(")); //Extraction du nom de la fonction candidate.
        if (name == nameM) { //Si les noms de la fonction candidate et du modèle correspondent, on compare le nombre d'arguments.
            occurencesM = 0;
            string mC = sm.c_str();
            for (int i = 0; i < sm.length(); i++) { //Nombre de virgule de la fonction candidate.
                if (mC[i] == ',') {
                    occurencesM++;
                }

            }

        }
        if (occurencesM == occurencesMod) { //Sachant que le nom correspont, si le nombre d'argument correspond, la fonction candidate est celle recherchée.
            this->m_fonction = it->second; //On stocke la fonction pour réutilisation.
            this->m_signature = it->first; //Et sa signature.
            /*
             Le nombre d'arguments de la fonction est le nombre de virgules + 1. Seul le cas du 0 argument peut porter à confusion avec celui du 1
             * argument. On vérifie donc s'il n'y a pas 0 arguments. (Parenthèses côte à côte)
             */
            this->m_nbArguments = (occurencesM == 0 && sm.find("(") == (sm.find(")") - 1)) ? occurencesM : occurencesM + 1;
            break;
        }
    }
}
